from tkinter import *
from tkinter import ttk


from elsapy.elsclient import ElsClient
from elsapy.elsprofile import ElsAuthor, ElsAffil
from elsapy.elsdoc import FullDoc, AbsDoc
from elsapy.elssearch import ElsSearch

import json

import scholarly

import xlrd
import os

#Scientometrics tausta jaoks

loendur = 1

def nimi_ilusaks(nimi):
    ilus_nimi = ""
            
    for sona in nimi.split(" "):
        if '-' in sona:
            ilus_nimi += sona.split('-')[0].capitalize() + '-' + sona.split('-')[1].capitalize() + ' '
        else:
            ilus_nimi += sona.capitalize() + " "
    
    return ilus_nimi.strip()


def otsi_ETIS_pub_ja_lisa_tabelisse(nimi, aasta_algus, aasta_lopp):
    teadustoode_loendur_ETIS_pub = 0
    nimi_olemas = False
    
    for root, dirs, files in os.walk("ETIS_data/teadustood/"):        
        for file in files:
             if file.endswith(".xlsx"):
                 aasta = int(file[0:4])
                 if aasta >= aasta_algus and aasta <= aasta_lopp:
                     etis_data_fail = xlrd.open_workbook(os.path.join(root, file))
                     etis_data = etis_data_fail.sheet_by_index(0)
                     for rida in range (1, etis_data.nrows):
                         teadustoode_loendur_ETIS_pub, nimi_olemas = lisa_tabelisse_ETIS_pub_andmed(etis_data, nimi, rida, teadustoode_loendur_ETIS_pub, nimi_olemas)
                     
    if nimi_olemas:
        tabel.set('nimi' + str(loendur), 'ETIS_pub_arv', teadustoode_loendur_ETIS_pub)
                        
    return nimi_olemas            

def lisa_tabelisse_ETIS_pub_andmed(etis_data, nimi, rida, teadustoode_loendur_ETIS_pub, nimi_olemas):
    autorite_list = tootle_nimed_ETIS_pub(etis_data, rida)
    ilus_nimi = nimi_ilusaks(nimi)
    
    if ilus_nimi in autorite_list:
        koik_klassid = False
        if etis_klas_11_var.get() == 0 and etis_klas_11_var.get() == 0 and etis_klas_11_var.get() == 0:
            koik_klassid = True
            
        if etis_klas_11_var.get() == 1 and etis_data.cell_value(rowx=rida, colx=30) == "1.1." or koik_klassid:
            lisa_tabeli_ritta_ETIS_pub_andmed(etis_data, rida, ilus_nimi, teadustoode_loendur_ETIS_pub)
            nimi_olemas = True
            teadustoode_loendur_ETIS_pub += 1
        
        elif etis_klas_12_var.get() == 1 and etis_data.cell_value(rowx=rida, colx=30) == "1.2." or koik_klassid:
            lisa_tabeli_ritta_ETIS_pub_andmed(etis_data, rida, ilus_nimi, teadustoode_loendur_ETIS_pub)
            nimi_olemas = True
            teadustoode_loendur_ETIS_pub += 1
        
        elif etis_klas_31_var.get() == 1 and etis_data.cell_value(rowx=rida, colx=30) == "3.1." or koik_klassid:
            lisa_tabeli_ritta_ETIS_pub_andmed(etis_data, rida, ilus_nimi, teadustoode_loendur_ETIS_pub)
            nimi_olemas = True
            teadustoode_loendur_ETIS_pub += 1
    
    return teadustoode_loendur_ETIS_pub, nimi_olemas
                        
def lisa_tabeli_ritta_ETIS_pub_andmed(etis_data, rida, ilus_nimi, teadustoode_loendur_ETIS_pub):
    if not tabel.exists('nimi' + str(loendur)):
        lisa_nimega_rida_tabelisse(ilus_nimi)
    if not tabel.exists('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_pub)):
        tabel.insert('nimi' + str(loendur), 'end', 'tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_pub), text = "")
    tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_pub), 'ETIS_pub', etis_data.cell_value(rowx=rida, colx=24) + " " + etis_data.cell_value(rowx=rida, colx=5))
    tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_pub), 'ETIS_pub_klas', etis_data.cell_value(rowx=rida, colx=30))
                  
def tootle_nimed_ETIS_pub(etis_data, rida):
    autorid = etis_data.cell_value(rowx=rida,colx=3).split(";")
    puhta_nimega_autorid = []
    
    for nimi in autorid:
        if nimi != "":
            nimi = nimi.split(",")
            perenimi = nimi[0].strip()
            eesnimi = nimi[1].replace('(Autor)', '').replace('(Toimetaja)', '').replace('(Tõlkija)', '').replace('(Koostaja)', '').strip()
            
            puhta_nimega_autorid.append(eesnimi + " " + perenimi)
    
    return puhta_nimega_autorid


def otsi_ETIS_proj_ja_lisa_tabelisse(nimi, nimi_olemas, aasta_algus, aasta_lopp):       
    teadustoode_loendur_ETIS_proj = 0
    raha_kokku = 0
    
    for root, dirs, files in os.walk("ETIS_data/projektid/"):
        for file in files:
            if file.endswith(".xlsx"):
                etis_data_fail = xlrd.open_workbook(os.path.join(root, file))
                etis_data = etis_data_fail.sheet_by_index(0)
                
                for rida in range(1,etis_data.nrows):
                    
                    if etis_data.cell_value(rowx=rida, colx=10)[-4:] != "":
                        aasta = int(etis_data.cell_value(rowx=rida, colx=10)[-4:])
                        
                        if aasta >= aasta_algus and aasta <= aasta_lopp:
                            kogu_nimi = tootle_nimi_ETIS_proj(etis_data, rida)
                            
                            if nimi_ilusaks(nimi) == kogu_nimi:
                                lisa_projekti_andmed(etis_data, teadustoode_loendur_ETIS_proj, rida, raha_kokku)    
                                nimi_olemas = True
                                raha_kokku += float(etis_data.cell_value(rowx=rida, colx=31).replace(',','.').replace(' ', ''))
                                teadustoode_loendur_ETIS_proj += 1
    
    if nimi_olemas:
        tabel.set('nimi' + str(loendur), 'ETIS_proj_arv', teadustoode_loendur_ETIS_proj)
        tabel.set('nimi' + str(loendur), 'ETIS_proj_rahasumma_kokku', raha_kokku)

    return nimi_olemas       

def tootle_nimi_ETIS_proj(etis_data, rida):
    tootlemata_nimi = etis_data.cell_value(rowx=rida,colx=17).split(",")
    
    if tootlemata_nimi[0] != '':
        perenimi = tootlemata_nimi[0]
        eesnimi = tootlemata_nimi[1]
        return (eesnimi + " " + perenimi).strip()
    
    return ''    
        
def lisa_projekti_andmed(etis_data, teadustoode_loendur_ETIS_proj, rida, raha_kokku):
    projektinimi = etis_data.cell_value(rowx=rida, colx=6)
    if projektinimi == '':
        projektinimi = etis_data.cell_value(rowx=rida, colx=7)
                            
    if not tabel.exists('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_proj)):
        tabel.insert('nimi' + str(loendur), 'end', 'tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_proj), text = "")
                                
    tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_proj), 'ETIS_proj', projektinimi)
                            
    aeg = etis_data.cell_value(rowx=rida, colx=10) + "-" + etis_data.cell_value(rowx=rida, colx=11)
    tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_proj), 'ETIS_proj_aeg', aeg)
    
    tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_proj), 'ETIS_proj_rahasumma', etis_data.cell_value(rowx=rida, colx=31))
    tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_ETIS_proj), 'ETIS_proj_finantsprog', etis_data.cell_value(rowx=rida, colx=21))
    
def google_scholar_autor(nimi):
    search_query = scholarly.search_author(nimi.strip())
    autor = next(search_query, '')
    
    if autor != '':
        return autor.fill()
    else:
        return ''

def lisa_tabelisse_GS_publikatsioonide_nimed(publikatsioonid, aasta_algus, aasta_lopp):
    teadustoode_loendur_gs = 0
    
    for pub in publikatsioonid:
        try:
            aasta = str(pub.bib['year']) + ' '
        except:
            aasta = ''
        
        if aasta != '' and aasta_algus <= int(aasta.strip()) and aasta_lopp >= int(aasta.strip()):
            if tabel.exists('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_gs)):
                tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_gs), 'GScholar', aasta + pub.bib['title'])
    
            else:
                tabel.insert('nimi' + str(loendur), 'end', 'tyhi' + str(loendur) + "_" + str(teadustoode_loendur_gs), text = "")
                tabel.set('tyhi' + str(loendur) + "_" + str(teadustoode_loendur_gs), 'GScholar', aasta + pub.bib['title'])
     
            
            teadustoode_loendur_gs +=1
            
    return teadustoode_loendur_gs

def lisa_tabelisse_GS_aastate_jargi_viited(gs_autor, aasta_algus, aasta_lopp):
    tsitaadid = gs_autor.cites_per_year
    
    tsitaatide_loendur_gs = 0
    kokku_tsitaate = 0
    
    for aasta in tsitaadid:
        if aasta_algus <= int(aasta) and aasta_lopp >= int(aasta):
            if tabel.exists('tyhi' + str(loendur) + "_" + str(tsitaatide_loendur_gs)):
                tabel.set('tyhi' + str(loendur) + "_" + str(tsitaatide_loendur_gs), 'GScholar_viited_aasta', str(aasta) + ': ' + str(tsitaadid.get(aasta)))
                
            else:
                tabel.insert('nimi' + str(loendur), 'end', 'tyhi' + str(loendur) + "_" + str(tsitaatide_loendur_gs), text = "")
                tabel.set('tyhi' + str(loendur) + "_" + str(tsitaatide_loendur_gs), 'GScholar_viited_aasta', str(aasta) + ': ' + str(tsitaadid.get(aasta)))
            
            tsitaatide_loendur_gs += 1
            kokku_tsitaate += tsitaadid.get(aasta)
            
    return kokku_tsitaate
            
def lisa_tabelisse_GS_andmed(gs_autor, aasta_algus, aasta_lopp):
    publikatsioonid = gs_autor.publications
    
    teadustoode_loendur_gs = lisa_tabelisse_GS_publikatsioonide_nimed(publikatsioonid, aasta_algus, aasta_lopp)
    
    kokku_tsitaate = lisa_tabelisse_GS_aastate_jargi_viited(gs_autor, aasta_algus, aasta_lopp)
        
    tabel.set('nimi' + str(loendur), 'GScholar_pub_arv', teadustoode_loendur_gs)
    
    if aasta_algus == 0 and aasta_lopp==9999:
        tabel.set('nimi' + str(loendur), 'GScholar_viited', gs_autor.citedby)
    else:
        tabel.set('nimi' + str(loendur), 'GScholar_viited', kokku_tsitaate)
    

def lisa_nimega_rida_tabelisse(nimi):
    ilus_nimi = nimi_ilusaks(nimi)
        
    tabel.insert('', str(loendur) , 'nimi' + str(loendur), text = ilus_nimi)


def scopus_autor(nimi):
    nimi_pooleks =nimi.lower().split()
    eesnimi = nimi_pooleks[0]
    perenimi = nimi_pooleks[1]
    
    konfiguratsiooni_fail = open("config.json")
    konfiguratsioon = json.load(konfiguratsiooni_fail)
    konfiguratsiooni_fail.close()

    klient = ElsClient(konfiguratsioon['apikey'])
    
    autori_otsing = ElsSearch('authlast(' + perenimi + ')authfirst(' + eesnimi + ')','author')
    try:
        autori_otsing.execute(klient)
    except:
        return ''
    
    if 'error' in autori_otsing.results[0]:
        return ''

    
    autori_id = autori_otsing.results[0]['dc:identifier'].split(':')[1]
    
    autor = ElsAuthor(uri = 'https://api.elsevier.com/content/author/author_id/' + autori_id)
    
    autor.read(klient)
    
    return autor

def lisa_tabelisse_Scopus_andmed(scp_autor):
    tabel.set('nimi' + str(loendur), 'Scopus_pub_arv', scp_autor.data['coredata']['document-count'])
    tabel.set('nimi' + str(loendur), 'Scopus_viited', scp_autor.data['coredata']['citation-count'])
    
def kas_otsida_koikidest_andmebaasidest():     
    if etis_pub_cb_var.get() == 0 and etis_proj_cb_var.get() == 0 and gs_cb_var.get() == 0 and scp_cb_var.get() == 0:
        return True
    return False
    
def leia_nimed_ja_lisa_tabelisse(nimede_list):   
    global loendur  
    
    
    if algus_aasta.get() == '':
        aasta_algus = 0
    else:
        aasta_algus = int(algus_aasta.get())
    
    if lopp_aasta.get() == '':
        aasta_lopp = 9999
    else:
        aasta_lopp = int(lopp_aasta.get())
        
    for nimi in nimede_list:
        if nimi == '':
            break
          
        koik_teadusandmebaasid_voimaldatud = kas_otsida_koikidest_andmebaasidest()
        
        nimi_olemas = False
        
        if etis_pub_cb_var.get() == 1 or koik_teadusandmebaasid_voimaldatud:
            nimi_olemas = otsi_ETIS_pub_ja_lisa_tabelisse(nimi, aasta_algus, aasta_lopp)
        
        if etis_proj_cb_var.get() == 1 or koik_teadusandmebaasid_voimaldatud:
            if not nimi_olemas:
                lisa_nimega_rida_tabelisse(nimi)
            
            nimi_olemas =  otsi_ETIS_proj_ja_lisa_tabelisse(nimi, nimi_olemas, aasta_algus, aasta_lopp)
        
        if gs_cb_var.get() == 1 or koik_teadusandmebaasid_voimaldatud:
            gs_autor = google_scholar_autor(nimi)
        
            if gs_autor != '':
                if not nimi_olemas:
                    lisa_nimega_rida_tabelisse(nimi)
                lisa_tabelisse_GS_andmed(gs_autor, aasta_algus, aasta_lopp)
                nimi_olemas = True
            
        if scp_cb_var.get() == 1 or koik_teadusandmebaasid_voimaldatud:
            scp_autor = scopus_autor(nimi)   
            if scp_autor != '':
                if not nimi_olemas:
                    lisa_nimega_rida_tabelisse(nimi)
                lisa_tabelisse_Scopus_andmed(scp_autor)
                nimi_olemas = True
        
        if not nimi_olemas:
            mitte_leitud_label = Label(raam, text='Sellise nimega autorit ei leitud')
            mitte_leitud_label.place(x = 1100, y = 350)
        loendur += 1
            
 
def otsi_nime_jargi():
    tyhi_label = Label(raam, text='                                                                                                             ')
    tyhi_label.place(x = 1100, y = 350)
    
    nimede_list = nimed.get().split(",")
    
    leia_nimed_ja_lisa_tabelisse(nimede_list)



raam = Tk()
raam.title("Akadeemilise töökoormuse kalkulaator")
raam.geometry("1600x500")

nime_silt = ttk.Label(raam, text="Sisesta nimi: ")
nime_silt.place(x=5, y=350)
nimed = ttk.Entry(raam)
nimed.place(x=100, y=350, width=150)

aasta_silt = ttk.Label(raam, text="Aastad:")
aasta_silt.place(x=905, y=350)

algus_aasta = ttk.Entry(raam)
algus_aasta.place(x= 950, y=350, width=40)

lopp_aasta = ttk.Entry(raam)
lopp_aasta.place(x= 995, y=350, width=40)

otsi_nupp = ttk.Button(raam, text="Otsi",  command=otsi_nime_jargi)
otsi_nupp.place(x=100, y=400)

##Checkboxid

etis_pub_cb_var = IntVar()
ETIS_pub_cb = ttk.Checkbutton(raam, text="ETIS publikatsioonid", variable=etis_pub_cb_var)
ETIS_pub_cb.place(x=600, y=300)

etis_klas_11_var = IntVar()
etis_klas_11 = ttk.Checkbutton(raam, text="klassifikatsioon 1.1", variable=etis_klas_11_var)
etis_klas_11.place(x=620, y=320)

etis_klas_12_var = IntVar()
etis_klas_12 = ttk.Checkbutton(raam, text="klassifikatsioon 1.2", variable=etis_klas_12_var)
etis_klas_12.place(x=620, y=340)

etis_klas_31_var = IntVar()
etis_klas_31 = ttk.Checkbutton(raam, text="klassifikatsioon 3.1", variable=etis_klas_31_var)
etis_klas_31.place(x=620, y=360)

etis_proj_cb_var = IntVar()
ETIS_proj_cb = ttk.Checkbutton(raam, text="ETIS projektid", variable=etis_proj_cb_var)
ETIS_proj_cb.place(x=700, y=400)

gs_cb_var = IntVar()
gs_cb = ttk.Checkbutton(raam, text="Google Scholar publikatsioonid\nNB! Selle otsingu päringud võivad\n võtta inimese kohta umbes\n 30 sekundit", variable=gs_cb_var)
gs_cb.place(x=400, y=300)

scp_cb_var = IntVar()
scp_cb = ttk.Checkbutton(raam, text="Scopus publikatsioonid\nNB! Sellele otsingule ei kehti aastapiirangud\nning kuvatakse autori info kõikide aastate kohta\nNB! Scopusest andmete saamiseks peab arvuti\n olema ühendatud Scopuse poolt aktsepteeritud\n võrku(nt Tartu Ülikooli eduroam)", variable=scp_cb_var)
scp_cb.place(x=400, y=400)

#Tabeli konfiguratsioon

tabel = ttk.Treeview(raam)
tabel.pack()

tabel.config(columns =('ETIS_pub', 'ETIS_pub_arv', 'ETIS_pub_klas', 'ETIS_proj', 'ETIS_proj_aeg', 'ETIS_proj_rahasumma' ,'ETIS_proj_finantsprog' , 'ETIS_proj_arv', 'ETIS_proj_rahasumma_kokku', 'GScholar' , 'GScholar_pub_arv', 'GScholar_viited', 'GScholar_viited_aasta', 'Scopus', 'Scopus_pub_arv', 'Scopus_viited'))
tabel.heading('#0', text='Teaduri nimi')
tabel.column('#0', width=120)



tabel.column('ETIS_pub', width=150)
tabel.heading('ETIS_pub', text= 'ETIS publ.')

tabel.column('ETIS_pub_arv', width=70)
tabel.heading('ETIS_pub_arv', text='Publ. arv')

tabel.column('ETIS_pub_klas', width=40)
tabel.heading('ETIS_pub_klas', text='Klassif.')

tabel.column('ETIS_proj', width=150)
tabel.heading('ETIS_proj', text= 'ETIS proj.')

tabel.column('ETIS_proj_aeg', width=119)
tabel.heading('ETIS_proj_aeg', text= 'ETIS proj kestus')

tabel.column('ETIS_proj_rahasumma', width=70)
tabel.heading('ETIS_proj_rahasumma', text= 'Rahasumma')

tabel.column('ETIS_proj_finantsprog', width=150)
tabel.heading('ETIS_proj_finantsprog', text= 'Finantsprogramm')

tabel.column('ETIS_proj_arv', width=70)
tabel.heading('ETIS_proj_arv', text= 'Proj. arv')

tabel.column('ETIS_proj_rahasumma_kokku', width=70)
tabel.heading('ETIS_proj_rahasumma_kokku', text= 'Rahasumma kokku')


tabel.column('GScholar', width=150)
tabel.heading('GScholar', text='Google Scholar')

tabel.column('GScholar_pub_arv', width=70)
tabel.heading('GScholar_pub_arv', text='Publ. arv')

tabel.column('GScholar_viited', width=70)
tabel.heading('GScholar_viited', text='Viidatud')

tabel.column('GScholar_viited_aasta', width=70)
tabel.heading('GScholar_viited_aasta', text='Aastate viited')

tabel.column('Scopus', width=70)
tabel.heading('Scopus', text='Scopus')

tabel.column('Scopus_pub_arv', width=55)
tabel.heading('Scopus_pub_arv', text='Publ. arv')

tabel.column('Scopus_viited', width=55)
tabel.heading('Scopus_viited', text='Viidatud')


vsb = ttk.Scrollbar(raam, orient="vertical", command=tabel.yview)
vsb.place(x=1575, y=0, height=220)

raam.mainloop()




